DROP TABLE IF EXISTS `dberp_ex_warehouse_order`;
CREATE TABLE IF NOT EXISTS `dberp_ex_warehouse_order` (
    `ex_warehouse_order_id` int(11) NOT NULL AUTO_INCREMENT,
    `warehouse_id` int(11) NOT NULL,
    `ex_warehouse_order_sn` varchar(50) NOT NULL,
    `ex_warehouse_order_state` tinyint(1) NOT NULL DEFAULT '6',
    `ex_warehouse_order_info` varchar(255) DEFAULT NULL,
    `ex_warehouse_order_goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
    `ex_warehouse_order_tax` decimal(19,4) NOT NULL DEFAULT '0.0000',
    `ex_warehouse_order_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
    `ex_add_time` int(10) NOT NULL,
    `admin_id` int(11) NOT NULL,
    PRIMARY KEY (`ex_warehouse_order_id`),
    KEY `warehouse_id` (`warehouse_id`),
    KEY `ex_warehouse_order_state` (`ex_warehouse_order_state`),
    KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='其他出库表';

DROP TABLE IF EXISTS `dberp_ex_warehouse_order_goods`;
CREATE TABLE IF NOT EXISTS `dberp_ex_warehouse_order_goods` (
    `ex_warehouse_order_goods_id` int(11) NOT NULL AUTO_INCREMENT,
    `ex_warehouse_order_id` int(11) NOT NULL,
    `warehouse_id` int(11) NOT NULL,
    `warehouse_goods_ex_num` int(11) NOT NULL,
    `warehouse_goods_price` decimal(19,4) NOT NULL DEFAULT '0.0000',
    `warehouse_goods_tax` decimal(19,4) NOT NULL DEFAULT '0.0000',
    `warehouse_goods_amount` decimal(19,4) NOT NULL DEFAULT '0.0000',
    `goods_id` int(11) NOT NULL,
    `goods_name` varchar(100) NOT NULL,
    `goods_number` varchar(30) NOT NULL,
    `goods_spec` varchar(100) DEFAULT NULL,
    `goods_unit` varchar(20) DEFAULT NULL,
    PRIMARY KEY (`ex_warehouse_order_goods_id`),
    KEY `warehouse_id` (`warehouse_id`),
    KEY `ex_warehouse_order_id` (`ex_warehouse_order_id`),
    KEY `goods_id` (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='其他出库商品表';