<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Store\Entity;

use Admin\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * 商品序列表
 * @ORM\Entity(repositoryClass="Store\Repository\GoodsSerialNumberRepository")
 * @ORM\Table(name="dberp_goods_serial_number")
 */
class GoodsSerialNumber extends BaseEntity
{
    /**
     * 自增id
     * @ORM\Id()
     * @ORM\Column(name="number_id", type="integer", length=11)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $numberId;

    /**
     * 序列号
     * @ORM\Column(name="serial_number", type="string", length=150)
     */
    private $serialNumber;

    /**
     * 序列号状态，1 未销售，2 已销售，3 已退货
     * @ORM\Column(name="serial_number_state", type="integer", length=1)
     */
    private $serialNumberState;

    /**
     * 商品id
     * @ORM\Column(name="goods_id", type="integer", length=11)
     */
    private $goodsId;

    /**
     * 仓库id
     * @ORM\Column(name="warehouse_id", type="integer", length=11)
     */
    private $warehouseId;

    /**
     * 类型，1 其他入库、2 其他出库、3 采购入库、4 销售出库
     * @ORM\Column(name="serial_number_type", type="integer", length=1)
     */
    private $serialNumberType;

    /**
     * 出库、入库 等id
     * @ORM\Column(name="outbound_in_id", type="integer", length=11)
     */
    private $outboundInId;

    /**
     * 出库时间
     * @ORM\Column(name="outbound_time", type="integer", length=10)
     */
    private $outboundTime = 0;

    /**
     * 入库时间
     * @ORM\Column(name="in_time", type="integer", length=10)
     */
    private $inTime = 0;

    /**
     * 退货类型，1 采购退货，2 销售退货
     * @ORM\Column(name="return_type", type="integer", length=1)
     */
    private $returnType = 0;

    /**
     * 退货时间
     * @ORM\Column(name="return_time", type="integer", length=10)
     */
    private $returnTime = 0;

    /**
     * 添加时间
     * @ORM\Column(name="add_time", type="integer", length=10)
     */
    private $addTime;

    /**
     * @return mixed
     */
    public function getNumberId()
    {
        return $this->numberId;
    }

    /**
     * @param mixed $numberId
     */
    public function setNumberId($numberId): void
    {
        $this->numberId = $numberId;
    }

    /**
     * @return mixed
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * @param mixed $serialNumber
     */
    public function setSerialNumber($serialNumber): void
    {
        $this->serialNumber = $serialNumber;
    }

    /**
     * @return mixed
     */
    public function getSerialNumberState()
    {
        return $this->serialNumberState;
    }

    /**
     * @param mixed $serialNumberState
     */
    public function setSerialNumberState($serialNumberState): void
    {
        $this->serialNumberState = $serialNumberState;
    }

    /**
     * @return mixed
     */
    public function getGoodsId()
    {
        return $this->goodsId;
    }

    /**
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId): void
    {
        $this->goodsId = $goodsId;
    }

    /**
     * @return mixed
     */
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }

    /**
     * @param mixed $warehouseId
     */
    public function setWarehouseId($warehouseId): void
    {
        $this->warehouseId = $warehouseId;
    }

    /**
     * @return mixed
     */
    public function getSerialNumberType()
    {
        return $this->serialNumberType;
    }

    /**
     * @param mixed $serialNumberType
     */
    public function setSerialNumberType($serialNumberType): void
    {
        $this->serialNumberType = $serialNumberType;
    }

    /**
     * @return mixed
     */
    public function getOutboundInId()
    {
        return $this->outboundInId;
    }

    /**
     * @param mixed $outboundInId
     */
    public function setOutboundInId($outboundInId): void
    {
        $this->outboundInId = $outboundInId;
    }

    /**
     * @return mixed
     */
    public function getOutboundTime()
    {
        return $this->outboundTime;
    }

    /**
     * @param mixed $outboundTime
     */
    public function setOutboundTime($outboundTime): void
    {
        $this->outboundTime = $outboundTime;
    }

    /**
     * @return mixed
     */
    public function getInTime()
    {
        return $this->inTime;
    }

    /**
     * @param mixed $inTime
     */
    public function setInTime($inTime): void
    {
        $this->inTime = $inTime;
    }

    /**
     * @return mixed
     */
    public function getReturnType()
    {
        return $this->returnType;
    }

    /**
     * @param mixed $returnType
     */
    public function setReturnType($returnType): void
    {
        $this->returnType = $returnType;
    }

    /**
     * @return mixed
     */
    public function getReturnTime()
    {
        return $this->returnTime;
    }

    /**
     * @param mixed $returnTime
     */
    public function setReturnTime($returnTime): void
    {
        $this->returnTime = $returnTime;
    }

    /**
     * @return mixed
     */
    public function getAddTime()
    {
        return $this->addTime;
    }

    /**
     * @param mixed $addTime
     */
    public function setAddTime($addTime): void
    {
        $this->addTime = $addTime;
    }
}