<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Store\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Store\Entity\Goods;
use Store\Entity\WarehouseGoods;

class WarehouseGoodsRepository extends EntityRepository
{
    /**
     * 库存商品列表
     * @param array $search
     * @return \Doctrine\ORM\Query
     */
    public function findWarehouseGoodsList(array $search = []): \Doctrine\ORM\Query
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('w', 'g.goodsName', 'g.goodsNumber', 'g.goodsSpec')
            ->from(WarehouseGoods::class, 'w')
            ->leftJoin(Goods::class, 'g', Join::WITH, 'w.goodsId=g.goodsId')
            ->orderBy('g.goodsSort', 'ASC');

        $query = $this->querySearchData($search, $query);

        return $query->getQuery();
    }

    /**
     * 仓库商品
     * @param $goodsId
     * @return mixed
     */
    public function findWarehouseGoods($goodsId)
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->select('g', 'w')
            ->from(WarehouseGoods::class, 'g')
            ->leftJoin('g.oneWarehouse', 'w')
            ->where('g.goodsId='.$goodsId)->andWhere('g.warehouseGoodsStock > 0');

        return $query->getQuery()->getResult();
    }

    /**
     * 查询商品数对应的仓库
     * @param $goodsId
     * @param $stockNum
     * @return float|int|mixed|string
     */
    /**
     * 查询商品数对应的仓库
     * @param $goodsId
     * @param $stockNum
     * @param array $warehouseIdArray
     * @return float|int|mixed|string
     */
    public function findWarehouseStockGoods($goodsId, $stockNum, array $warehouseIdArray = [])
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('g')
            ->from(WarehouseGoods::class, 'g')
            ->where($query->expr()->eq('g.goodsId', $goodsId))
            ->andWhere($query->expr()->gte('g.warehouseGoodsStock', $stockNum));
        if (!empty($warehouseIdArray)) $query->andWhere($query->expr()->in('g.warehouseId', $warehouseIdArray));

        return $query->getQuery()->getResult();
    }

    /**
     * 获取指定仓库里的商品总数
     * @param $warehouseId
     * @return float|int|mixed|string
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findWarehouseGoodsStock($warehouseId)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('SUM(wg.warehouseGoodsStock)')
            ->from(WarehouseGoods::class, 'wg')
            ->where('wg.warehouseId = :warehouseId')->setParameter('warehouseId', $warehouseId);

        $stockNum = $query->getQuery()->getSingleScalarResult();

        return $stockNum ?: 0;
    }

    /**
     * 获取同一个商品在不同仓库的库存
     * @param array $warehouseId
     * @param $goodsId
     * @return int|mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findMoreWarehouseGoodsNum(array $warehouseId, $goodsId)
    {
        $query = $this->getEntityManager()->createQuery(
            '
                  SELECT SUM(wg.warehouseGoodsStock) FROM Store\Entity\WarehouseGoods wg WHERE wg.warehouseId IN ('. implode(',', $warehouseId) .')
                  and wg.goodsId='.$goodsId
        );

        $stockNum = $query->getSingleScalarResult();

        return $stockNum ?: 0;
    }

    private function querySearchData($search, QueryBuilder $queryBuilder): QueryBuilder
    {
        if(!empty($search['goods_name']))                                   $queryBuilder->andWhere($queryBuilder->expr()->like('g.goodsName', "'%".$search['goods_name']."%'"));
        if(!empty($search['goods_number']))                                 $queryBuilder->andWhere($queryBuilder->expr()->like('g.goodsNumber', "'%".$search['goods_number']."%'"));
        if(!empty($search['goods_spec']))                                   $queryBuilder->andWhere($queryBuilder->expr()->like('g.goodsSpec', "'%".$search['goods_spec']."%'"));
        if(isset($search['start_stock']) && $search['start_stock'] > 0)     $queryBuilder->andWhere($queryBuilder->expr()->gte('w.warehouseGoodsStock', $search['start_stock']));
        if(isset($search['end_stock']) && $search['end_stock'] > 0)         $queryBuilder->andWhere($queryBuilder->expr()->lte('w.warehouseGoodsStock', $search['end_stock']));
        if(isset($search['warehouse_id']) && $search['warehouse_id'] > 0)   $queryBuilder->andWhere($queryBuilder->expr()->eq('w.warehouseId', $search['warehouse_id']));

        return $queryBuilder;
    }
}