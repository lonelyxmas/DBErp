<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Store\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Store\Entity\Goods;
use Store\Entity\GoodsSerialNumber;
use Store\Entity\Warehouse;

class GoodsSerialNumberRepository extends EntityRepository
{
    public function goodsSerialNumberList($search = []): \Doctrine\ORM\Query
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('n')
            ->addSelect('(SELECT g.goodsName FROM '.Goods::class.' AS g WHERE g.goodsId=n.goodsId) AS goodsName')
            ->addSelect('(SELECT w.warehouseName FROM '.Warehouse::class.' AS w WHERE w.warehouseId=n.warehouseId) AS warehouseName')
            ->from(GoodsSerialNumber::class, 'n')
            ->orderBy('n.addTime', 'DESC');

        $query = $this->querySearchData($search, $query);

        return $query->getQuery();
    }
    /**
     * ajax商品序列号检测
     * @param $goodsId
     * @param $searchGoodsSerialNumber
     * @param $searchWarehouseId
     * @return \Doctrine\ORM\Query
     */
    public function ajaxFindAllGoodsSerialNumber($goodsId, $searchGoodsSerialNumber, $searchWarehouseId = 0): \Doctrine\ORM\Query
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('n')
            ->addSelect('(SELECT g.goodsName FROM '.Goods::class.' AS g WHERE g.goodsId=n.goodsId) AS goodsName')
            ->addSelect('(SELECT w.warehouseName FROM '.Warehouse::class.' AS w WHERE w.warehouseId=n.warehouseId) AS warehouseName')
            ->from(GoodsSerialNumber::class, 'n')
            ->where('n.goodsId = :goodsId')->setParameter('goodsId', $goodsId)
            ->andWhere('n.serialNumberState = :serialNumberState')->setParameter('serialNumberState', 1);

        if (!empty($searchGoodsSerialNumber)) $query->andWhere($query->expr()->like('n.serialNumber', "'%".$searchGoodsSerialNumber."%'"));
        if ($searchWarehouseId > 0) $query->andWhere('n.warehouseId = :warehouseId')->setParameter('warehouseId', $searchWarehouseId);

        return $query->getQuery();
    }

    /**
     * 根据仓库检索序列号
     * @param $goodsId
     * @param array $warehouseId
     * @param $state
     * @return float|int|mixed|string
     */
    public function ajaxFindGoodsSerialNumberByWarehouseId($goodsId, array $warehouseId, $state = 1)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('n')
            ->from(GoodsSerialNumber::class, 'n')
            ->where('n.goodsId = :goodsId')->setParameter('goodsId', $goodsId)
            ->andWhere('n.serialNumberState = :serialNumberState')->setParameter('serialNumberState', $state)
            ->andWhere($query->expr()->in('n.warehouseId', $warehouseId));

        return $query->getQuery()->getResult();
    }

    /**
     * 根据序列号组获取序列号信息
     * @param array $serialNumber
     * @param $goodsId
     * @param $warehouseId
     * @return float|int|mixed[]|string
     */
    public function findGoodsSerialNumberBySerialNumber(array $serialNumber, $goodsId, $warehouseId = 0)
    {
        $query = $this->getEntityManager()->createQueryBuilder();
        $query->select('n')
            ->from(GoodsSerialNumber::class, 'n')
            ->where('n.goodsId = :goodsId')->setParameter('goodsId', $goodsId)
            ->andWhere('n.serialNumberState = :serialNumberState')->setParameter('serialNumberState', 1)
            ->andWhere($query->expr()->in('n.serialNumber', $serialNumber));

        if ($warehouseId > 0) $query->andWhere('n.warehouseId = :warehouseId')->setParameter('warehouseId', $warehouseId);

        return $query->getQuery()->getResult();

    }

    /**
     * 对检索信息进行处理
     * @param $search
     * @param QueryBuilder $queryBuilder
     * @return QueryBuilder
     */
    private function querySearchData($search, QueryBuilder $queryBuilder): QueryBuilder
    {
        if (!empty($search['serial_number'])) $queryBuilder->andWhere($queryBuilder->expr()->like('n.serialNumber', "'%".$search['serial_number']."%'"));
        if (!empty($search['goods_name'])) $queryBuilder->having('goodsName = :goodsName')->setParameter('goodsName', $search['goods_name']);
        if (!empty($search['warehouse_name'])) $queryBuilder->andHaving('warehouseName = :warehouseName')->setParameter('warehouseName', $search['warehouse_name']);
        if (isset($search['serial_number_state']) && $search['serial_number_state'] > 0) $queryBuilder->andWhere($queryBuilder->expr()->eq('n.serialNumberState', ':serialNumberState'))->setParameter('serialNumberState', $search['serial_number_state']);
        if (!empty($search['o_start_time'])) $queryBuilder->andWhere($queryBuilder->expr()->gte('n.outboundTime', ':startTime'))->setParameter('startTime', strtotime($search['o_start_time']));
        if (!empty($search['o_end_time'])) $queryBuilder->andWhere($queryBuilder->expr()->lte('n.outboundTime', ':endTime'))->setParameter('endTime', strtotime($search['o_end_time']));
        if (!empty($search['i_start_time'])) $queryBuilder->andWhere($queryBuilder->expr()->gte('n.addTime', ':startTime'))->setParameter('startTime', strtotime($search['i_start_time']));
        if (!empty($search['i_end_time'])) $queryBuilder->andWhere($queryBuilder->expr()->lte('n.addTime', ':endTime'))->setParameter('endTime', strtotime($search['i_end_time']));

        return $queryBuilder;
    }
}