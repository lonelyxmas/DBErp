<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Admin\Filter;

use Laminas\Filter\AbstractFilter;

class OldImage extends AbstractFilter
{
    private $oldImage;

    public function __construct($options = [])
    {
        $this->oldImage = $this->checkOldImage($options['oldImage']);
    }

    public function filter($value)
    {
        if(!empty($value['tmp_name']) && !empty($this->oldImage) && file_exists(getcwd() . '/public/' . $this->oldImage)) @unlink(getcwd() . '/public/' . $this->oldImage);
        return $value;
    }

    /**
     * 对图片的路径进行处理
     * @param $oldImage
     * @return string
     */
    private function checkOldImage($oldImage)
    {
        if(empty($oldImage)) return '';

        $imagePath = dirname($oldImage);
        $imagePath = empty($imagePath) ? '' : str_replace('.', '', $imagePath) . '/';
        $imageName = basename($oldImage);

        return $imagePath . $imageName;
    }
}