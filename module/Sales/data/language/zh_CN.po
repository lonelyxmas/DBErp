msgid ""
msgstr ""
"Project-Id-Version: DBErp\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-06-19 18:23+0800\n"
"PO-Revision-Date: 2023-06-19 18:23+0800\n"
"Last-Translator: Baron <baron@loongdom.cn>\n"
"Language-Team: DBErp <baron@loongdom.cn>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: translate\n"
"X-Poedit-Basepath: ../..\n"
"X-Generator: Poedit 3.3.1\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"

#: src/Controller/SalesOrderController.php:132
msgid "销售订单添加成功！"
msgstr ""

#: src/Controller/SalesOrderController.php:133
#: src/Controller/SalesOrderController.php:184
#: src/Controller/SalesOrderController.php:238
#: src/Controller/SalesOrderController.php:264
#: src/Controller/SalesOrderController.php:286
#: src/Controller/SalesOrderController.php:329
#: src/Controller/SalesOrderController.php:388
#: view/sales/sales-order/view.phtml:23 view/sales/translate.phtml:4
msgid "销售订单"
msgstr ""

#: src/Controller/SalesOrderController.php:136
msgid "销售订单添加失败！"
msgstr ""

#: src/Controller/SalesOrderController.php:154
#: src/Controller/SalesOrderController.php:207
#: src/Controller/SalesOrderController.php:301
msgid "该订单不存在！"
msgstr ""

#: src/Controller/SalesOrderController.php:158
msgid "该订单所属状态不能被编辑！"
msgstr ""

#: src/Controller/SalesOrderController.php:183
msgid "销售订单编辑成功！"
msgstr ""

#: src/Controller/SalesOrderController.php:187
msgid "销售订单编辑失败！"
msgstr ""

#: src/Controller/SalesOrderController.php:237
msgid "销售订单删除成功！"
msgstr ""

#: src/Controller/SalesOrderController.php:241
msgid "销售订单删除失败！"
msgstr ""

#: src/Controller/SalesOrderController.php:258
msgid "确认采购单失败！"
msgstr ""

#: src/Controller/SalesOrderController.php:263
msgid "销售订单确认完成！"
msgstr ""

#: src/Controller/SalesOrderController.php:280
msgid "取消确认采购单失败！"
msgstr ""

#: src/Controller/SalesOrderController.php:285
msgid "销售订单取消确认完成！"
msgstr ""

#: src/Controller/SalesOrderController.php:328
msgid "销售订单发货成功！"
msgstr ""

#: src/Controller/SalesOrderController.php:332
msgid "销售订单发货失败！"
msgstr ""

#: src/Controller/SalesOrderController.php:388
msgid "订单，删除商品"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:103
#: src/Controller/SalesSendOrderController.php:83
#: src/Controller/SalesSendOrderController.php:117
msgid "该发货单不存在！"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:107
#: src/Controller/SalesSendOrderController.php:121
msgid "当前状态不能执行该操作"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:137
#: src/Controller/SalesOrderReturnController.php:220
#: src/Controller/SalesOrderReturnController.php:277
#: src/Controller/SalesSendOrderController.php:129
msgid "销售单"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:137
msgid "退货添加完成！"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:138
#: src/Controller/SalesOrderReturnController.php:221
#: src/Controller/SalesOrderReturnController.php:278
#: view/sales/translate.phtml:19
msgid "销售退货"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:142
msgid "退货添加失败！"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:189
#: src/Controller/SalesOrderReturnController.php:210
#: src/Controller/SalesOrderReturnController.php:238
msgid "该销售退货单不存在！"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:213
msgid "该销售退货不能执行完成操作！"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:220
msgid "退货操作完成！"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:241
msgid "该销售退货不能执行取消操作！"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:277
msgid "取消退货成功！"
msgstr ""

#: src/Controller/SalesOrderReturnController.php:280
msgid "取消退货失败！"
msgstr ""

#: src/Controller/SalesSendOrderController.php:129
msgid "订单确认收货完成！"
msgstr ""

#: src/Controller/SalesSendOrderController.php:130
msgid "销售确认收货"
msgstr ""

#: src/Form/SalesOrderReturnForm.php:94
#: src/Validator/SalesOrderGoodsIdReturnValidator.php:38
#: view/sales/sales-order-return/add.phtml:133
msgid "请选择退货商品"
msgstr ""

#: src/Form/SearchSalesOrderForm.php:45
#: src/Form/SearchSalesOrderReturnForm.php:45
#: src/Form/SearchSendOrderForm.php:45
msgid "起始金额"
msgstr ""

#: src/Form/SearchSalesOrderForm.php:55
#: src/Form/SearchSalesOrderReturnForm.php:55
#: src/Form/SearchSendOrderForm.php:55
msgid "结束金额"
msgstr ""

#: src/Form/SearchSalesOrderForm.php:65
#: src/Form/SearchSalesOrderReturnForm.php:65
#: view/sales/sales-order-return/add.phtml:47
#: view/sales/sales-order-return/index.phtml:23
#: view/sales/sales-order-return/view.phtml:38
#: view/sales/sales-order/add.phtml:12 view/sales/sales-order/add.phtml:44
#: view/sales/sales-order/index.phtml:24
#: view/sales/sales-order/send-order.phtml:30
#: view/sales/sales-order/view.phtml:30
#: view/sales/sales-send-order/view.phtml:38
msgid "销售单号"
msgstr ""

#: src/Form/SearchSalesOrderForm.php:75
#: src/Form/SearchSalesOrderReturnForm.php:85
#: src/Form/SearchSendOrderForm.php:75
#: view/sales/sales-order-return/index.phtml:26
#: view/sales/sales-order/index.phtml:26
#: view/sales/sales-send-order/index.phtml:25
msgid "联系人"
msgstr ""

#: src/Form/SearchSalesOrderForm.php:85
#: src/Form/SearchSalesOrderReturnForm.php:95
#: src/Form/SearchSendOrderForm.php:85
msgid "客户电话"
msgstr ""

#: src/Form/SearchSalesOrderReturnForm.php:75
#: src/Form/SearchSendOrderForm.php:65
#: view/sales/sales-order-return/add.phtml:39
#: view/sales/sales-order-return/index.phtml:24
#: view/sales/sales-order-return/view.phtml:30
#: view/sales/sales-order/send-order.phtml:89
#: view/sales/sales-send-order/index.phtml:23
#: view/sales/sales-send-order/view.phtml:30
msgid "发货单号"
msgstr ""

#: src/Validator/SalesOrderCodeValidator.php:40
#: src/Validator/SalesOrderGoodsReturnAmountValidator.php:38
#: src/Validator/SalesOrderGoodsReturnNumValidator.php:40
#: src/Validator/SendOrderSnValidator.php:36
#: src/Validator/SendOrderWarehouseValidator.php:40
msgid "这不是一个标准输入值"
msgstr ""

#: src/Validator/SalesOrderCodeValidator.php:41
msgid "该销售订单号已经存在"
msgstr ""

#: src/Validator/SalesOrderGoodsArrayValidator.php:49
msgid "不能为空"
msgstr ""

#: src/Validator/SalesOrderGoodsArrayValidator.php:50
msgid "商品不存在"
msgstr ""

#: src/Validator/SalesOrderGoodsArrayValidator.php:51
msgid "商品销售单价不能为负数"
msgstr ""

#: src/Validator/SalesOrderGoodsArrayValidator.php:52
msgid "税金不能为负数"
msgstr ""

#: src/Validator/SalesOrderGoodsArrayValidator.php:53
msgid "销售数量不能小于等于0"
msgstr ""

#: src/Validator/SalesOrderGoodsArrayValidator.php:54
msgid "商品总价不能为负数"
msgstr ""

#: src/Validator/SalesOrderGoodsIdReturnValidator.php:39
msgid "有些商品不在该销售单中"
msgstr ""

#: src/Validator/SalesOrderGoodsReturnAmountValidator.php:39
msgid "商品退货金额超出标准金额"
msgstr ""

#: src/Validator/SalesOrderGoodsReturnAmountValidator.php:40
msgid "退货金额不能为负数"
msgstr ""

#: src/Validator/SalesOrderGoodsReturnAmountValidator.php:41
msgid "退货金额不是一个标准的数值"
msgstr ""

#: src/Validator/SalesOrderGoodsReturnNumValidator.php:41
msgid "退货数量超过标准数量"
msgstr ""

#: src/Validator/SalesOrderGoodsReturnNumValidator.php:42
msgid "退货数量不能为负数"
msgstr ""

#: src/Validator/SalesOrderGoodsReturnNumValidator.php:43
msgid "退货数量不是一个标准的数字"
msgstr ""

#: src/Validator/SalesOrderGoodsReturnNumValidator.php:44
msgid "退货数量不能为0"
msgstr ""

#: src/Validator/SalesOrderGoodsReturnNumValidator.php:45
msgid "退货序列号数量与退货数量不相符"
msgstr ""

#: src/Validator/SendOrderSnValidator.php:37
msgid "该发货单号已经存在"
msgstr ""

#: src/Validator/SendOrderWarehouseValidator.php:41
msgid "库存不足，无法发货"
msgstr ""

#: src/Validator/SendOrderWarehouseValidator.php:42
msgid "无足够的商品序列号出库"
msgstr ""

#: view/sales/sales-order-return/add.phtml:20
msgid "返回销售发货单列表"
msgstr ""

#: view/sales/sales-order-return/add.phtml:21
#: view/sales/sales-order-return/add.phtml:180
msgid "点击退货"
msgstr ""

#: view/sales/sales-order-return/add.phtml:32
msgid "销售订单信息"
msgstr ""

#: view/sales/sales-order-return/add.phtml:53
#: view/sales/sales-order/send-order.phtml:36
#: view/sales/sales-order/view.phtml:36
#: view/sales/sales-send-order/view.phtml:44
msgid "订单状态"
msgstr ""

#: view/sales/sales-order-return/add.phtml:59
#: view/sales/sales-order-return/view.phtml:50
#: view/sales/sales-order/send-order.phtml:42
#: view/sales/sales-order/view.phtml:42
#: view/sales/sales-send-order/view.phtml:50
msgid "付款方式"
msgstr ""

#: view/sales/sales-order-return/add.phtml:68
#: view/sales/sales-order-return/index.phtml:25
#: view/sales/sales-order-return/view.phtml:59
#: view/sales/sales-order/add.phtml:52 view/sales/sales-order/index.phtml:25
#: view/sales/sales-order/send-order.phtml:51
#: view/sales/sales-order/view.phtml:51
#: view/sales/sales-send-order/index.phtml:24
#: view/sales/sales-send-order/view.phtml:59
msgid "客户"
msgstr ""

#: view/sales/sales-order-return/add.phtml:73
#: view/sales/sales-order-return/view.phtml:64
#: view/sales/sales-order/add.phtml:71
#: view/sales/sales-order/send-order.phtml:56
#: view/sales/sales-order/view.phtml:56
#: view/sales/sales-send-order/view.phtml:64
msgid "客户联系人"
msgstr ""

#: view/sales/sales-order-return/add.phtml:79
#: view/sales/sales-order-return/view.phtml:70
#: view/sales/sales-order/add.phtml:79
#: view/sales/sales-order/send-order.phtml:62
#: view/sales/sales-order/view.phtml:62
#: view/sales/sales-send-order/view.phtml:70
msgid "客户手机"
msgstr ""

#: view/sales/sales-order-return/add.phtml:88
#: view/sales/sales-order-return/view.phtml:79
#: view/sales/sales-order/add.phtml:87
#: view/sales/sales-order/send-order.phtml:71
#: view/sales/sales-order/view.phtml:71
#: view/sales/sales-send-order/view.phtml:79
msgid "客户座机"
msgstr ""

#: view/sales/sales-order-return/add.phtml:93
#: view/sales/sales-order-return/view.phtml:84
#: view/sales/sales-order/send-order.phtml:76
#: view/sales/sales-order/view.phtml:76
#: view/sales/sales-send-order/view.phtml:84
msgid "收货地址"
msgstr ""

#: view/sales/sales-order-return/add.phtml:98
#: view/sales/sales-order/add.phtml:16 view/sales/sales-order/add.phtml:105
#: view/sales/sales-order/send-order.phtml:81
#: view/sales/sales-order/view.phtml:81
#: view/sales/sales-send-order/view.phtml:89
msgid "备注"
msgstr ""

#: view/sales/sales-order-return/add.phtml:104
msgid "选择退货商品"
msgstr ""

#: view/sales/sales-order-return/add.phtml:116
msgid "选择"
msgstr ""

#: view/sales/sales-order-return/add.phtml:117
#: view/sales/sales-order-return/view.phtml:105
#: view/sales/sales-order/add.phtml:129
#: view/sales/sales-order/send-order.phtml:108
#: view/sales/sales-order/view.phtml:93
#: view/sales/sales-send-order/view.phtml:101
msgid "商品编号"
msgstr ""

#: view/sales/sales-order-return/add.phtml:118
#: view/sales/sales-order-return/view.phtml:106
#: view/sales/sales-order/add.phtml:130
#: view/sales/sales-order/send-order.phtml:109
#: view/sales/sales-order/view.phtml:94
#: view/sales/sales-send-order/view.phtml:102
msgid "商品名称"
msgstr ""

#: view/sales/sales-order-return/add.phtml:119
#: view/sales/sales-order-return/view.phtml:107
#: view/sales/sales-order/add.phtml:131
#: view/sales/sales-order/send-order.phtml:110
#: view/sales/sales-order/view.phtml:95
#: view/sales/sales-send-order/view.phtml:103
msgid "商品规格"
msgstr ""

#: view/sales/sales-order-return/add.phtml:120
#: view/sales/sales-order-return/view.phtml:109
#: view/sales/sales-order/add.phtml:133
#: view/sales/sales-order/send-order.phtml:112
#: view/sales/sales-order/view.phtml:97
#: view/sales/sales-send-order/view.phtml:105
msgid "销售单价"
msgstr ""

#: view/sales/sales-order-return/add.phtml:121
#: view/sales/sales-order/add.phtml:134
#: view/sales/sales-order/send-order.phtml:113
#: view/sales/sales-order/view.phtml:98
#: view/sales/sales-send-order/view.phtml:106
msgid "销售数量"
msgstr ""

#: view/sales/sales-order-return/add.phtml:122
#: view/sales/sales-order/add.phtml:135
#: view/sales/sales-order/send-order.phtml:114
#: view/sales/sales-order/view.phtml:99
#: view/sales/sales-send-order/view.phtml:107
msgid "税金"
msgstr ""

#: view/sales/sales-order-return/add.phtml:123
#: view/sales/sales-order/add.phtml:136
#: view/sales/sales-order/send-order.phtml:115
#: view/sales/sales-order/view.phtml:100
#: view/sales/sales-send-order/view.phtml:108
msgid "销售总价"
msgstr ""

#: view/sales/sales-order-return/add.phtml:124
#: view/sales/sales-order-return/view.phtml:110
msgid "退货数量"
msgstr ""

#: view/sales/sales-order-return/add.phtml:125
#: view/sales/sales-order-return/index.phtml:28
msgid "退货金额"
msgstr ""

#: view/sales/sales-order-return/add.phtml:138
#: view/sales/sales-order-return/view.phtml:123
#: view/sales/sales-order/add.phtml:153 view/sales/sales-order/add.phtml:288
#: view/sales/sales-order/send-order.phtml:126
#: view/sales/sales-send-order/view.phtml:120
msgid "序列号"
msgstr ""

#: view/sales/sales-order-return/add.phtml:171
msgid "退货原因"
msgstr ""

#: view/sales/sales-order-return/add.phtml:208
msgid "商品序列号"
msgstr ""

#: view/sales/sales-order-return/add.phtml:208
#: view/sales/sales-order-return/add.phtml:209
msgid "快速检索"
msgstr ""

#: view/sales/sales-order-return/add.phtml:209
#: view/sales/sales-order-return/view.phtml:135
msgid "退货序列号"
msgstr ""

#: view/sales/sales-order-return/index.phtml:23
#: view/sales/sales-order/index.phtml:24
#: view/sales/sales-send-order/index.phtml:23
msgid "订单数："
msgstr ""

#: view/sales/sales-order-return/index.phtml:27
#: view/sales/sales-order/index.phtml:27
#: view/sales/sales-send-order/index.phtml:26
msgid "联系电话"
msgstr ""

#: view/sales/sales-order-return/index.phtml:29
#: view/sales/sales-order/index.phtml:30 view/sales/sales-order/view.phtml:134
#: view/sales/sales-send-order/index.phtml:29
msgid "状态"
msgstr ""

#: view/sales/sales-order-return/index.phtml:30
#: view/sales/sales-order/add.phtml:137 view/sales/sales-order/index.phtml:32
#: view/sales/sales-send-order/index.phtml:31
msgid "操作"
msgstr ""

#: view/sales/sales-order-return/index.phtml:94
#: view/sales/sales-order/index.phtml:118
#: view/sales/sales-send-order/index.phtml:104
msgid "查看"
msgstr ""

#: view/sales/sales-order-return/index.phtml:97 view/sales/translate.phtml:23
msgid "退货完成"
msgstr ""

#: view/sales/sales-order-return/index.phtml:99 view/sales/translate.phtml:24
msgid "取消退货"
msgstr ""

#: view/sales/sales-order-return/view.phtml:11
msgid "返回退货单列表"
msgstr ""

#: view/sales/sales-order-return/view.phtml:12
#: view/sales/sales-order/view.phtml:12
#: view/sales/sales-send-order/view.phtml:12
msgid "点击打印"
msgstr ""

#: view/sales/sales-order-return/view.phtml:23
msgid "销售退货单"
msgstr ""

#: view/sales/sales-order-return/view.phtml:44
msgid "退货状态"
msgstr ""

#: view/sales/sales-order-return/view.phtml:93
msgid "退货备注"
msgstr ""

#: view/sales/sales-order-return/view.phtml:99
msgid "退货商品"
msgstr ""

#: view/sales/sales-order-return/view.phtml:108
#: view/sales/sales-order/add.phtml:132
#: view/sales/sales-order/send-order.phtml:111
#: view/sales/sales-order/view.phtml:96
#: view/sales/sales-send-order/view.phtml:104
msgid "单位"
msgstr ""

#: view/sales/sales-order-return/view.phtml:111
msgid "退货总价"
msgstr ""

#: view/sales/sales-order-return/view.phtml:143
msgid "退货金额合计"
msgstr ""

#: view/sales/sales-order/add.phtml:25
#: view/sales/sales-order/send-order.phtml:11
#: view/sales/sales-order/view.phtml:11
msgid "返回销售订单列表"
msgstr ""

#: view/sales/sales-order/add.phtml:26 view/sales/sales-order/add.phtml:180
msgid "保存销售订单"
msgstr ""

#: view/sales/sales-order/add.phtml:37
msgid "编辑销售订单"
msgstr ""

#: view/sales/sales-order/add.phtml:37 view/sales/sales-order/index.phtml:7
msgid "添加销售订单"
msgstr ""

#: view/sales/sales-order/add.phtml:60 view/sales/sales-order/index.phtml:28
#: view/sales/sales-send-order/index.phtml:27
msgid "收款方式"
msgstr ""

#: view/sales/sales-order/add.phtml:98
msgid "客户地址"
msgstr ""

#: view/sales/sales-order/add.phtml:117 view/sales/sales-order/view.phtml:87
#: view/sales/sales-send-order/view.phtml:95
msgid "销售商品"
msgstr ""

#: view/sales/sales-order/add.phtml:119
msgid "点击添加商品"
msgstr ""

#: view/sales/sales-order/add.phtml:163 view/sales/sales-order/add.phtml:298
#: view/sales/sales-order/index.phtml:112
msgid "删除"
msgstr ""

#: view/sales/sales-order/add.phtml:173
#: view/sales/sales-order/send-order.phtml:157
#: view/sales/sales-order/view.phtml:121
#: view/sales/sales-send-order/view.phtml:146
msgid "销售金额合计"
msgstr ""

#: view/sales/sales-order/add.phtml:223
msgid "销售单号不能为空！"
msgstr ""

#: view/sales/sales-order/add.phtml:226 view/sales/sales-order/add.phtml:227
msgid "请选择客户！"
msgstr ""

#: view/sales/sales-order/add.phtml:230
msgid "请选择收款方式！"
msgstr ""

#: view/sales/sales-order/add.phtml:233
msgid "请填写客户地址！"
msgstr ""

#: view/sales/sales-order/add.phtml:236
msgid "请输入联系人！"
msgstr ""

#: view/sales/sales-order/add.phtml:241
msgid "请添加销售商品！"
msgstr ""

#: view/sales/sales-order/add.phtml:267
msgid "请填写需要添加的商品名称！"
msgstr ""

#: view/sales/sales-order/add.phtml:276
msgid "该商品已经添加！"
msgstr ""

#: view/sales/sales-order/add.phtml:338
msgid "商品删除失败！"
msgstr ""

#: view/sales/sales-order/index.phtml:29
#: view/sales/sales-send-order/index.phtml:28
msgid "收款金额"
msgstr ""

#: view/sales/sales-order/index.phtml:31
#: view/sales/sales-send-order/index.phtml:30
#: view/sales/sales-send-order/index.phtml:109
msgid "退货"
msgstr ""

#: view/sales/sales-order/index.phtml:103
#: view/sales/sales-send-order/index.phtml:100
msgid "有"
msgstr ""

#: view/sales/sales-order/index.phtml:103
#: view/sales/sales-send-order/index.phtml:100
msgid "无"
msgstr ""

#: view/sales/sales-order/index.phtml:108
msgid "编辑"
msgstr ""

#: view/sales/sales-order/index.phtml:110
msgid "确认"
msgstr ""

#: view/sales/sales-order/index.phtml:111
msgid "您确实要删除该销售单吗？"
msgstr ""

#: view/sales/sales-order/index.phtml:123
msgid "发货出库"
msgstr ""

#: view/sales/sales-order/index.phtml:124 view/sales/translate.phtml:13
msgid "取消确认"
msgstr ""

#: view/sales/sales-order/send-order.phtml:12
#: view/sales/sales-order/send-order.phtml:164
msgid "点击发货出库"
msgstr ""

#: view/sales/sales-order/send-order.phtml:23
msgid "订单发货出库"
msgstr ""

#: view/sales/sales-order/send-order.phtml:96
msgid "发货商品"
msgstr ""

#: view/sales/sales-order/send-order.phtml:101
msgid "批量选择仓库"
msgstr ""

#: view/sales/sales-order/send-order.phtml:139
#: view/sales/sales-order/send-order.phtml:148
msgid "选择发货仓库"
msgstr ""

#: view/sales/sales-order/send-order.phtml:141
msgid "请选择仓库"
msgstr ""

#: view/sales/sales-order/send-order.phtml:141
msgid "库存："
msgstr ""

#: view/sales/sales-order/send-order.phtml:149
msgid "仓库无库存"
msgstr ""

#: view/sales/sales-order/view.phtml:128
msgid "操作记录"
msgstr ""

#: view/sales/sales-order/view.phtml:135
msgid "操作人"
msgstr ""

#: view/sales/sales-order/view.phtml:136
msgid "操作时间"
msgstr ""

#: view/sales/sales-send-order/index.phtml:107 view/sales/translate.phtml:18
msgid "确认收货"
msgstr ""

#: view/sales/sales-send-order/view.phtml:11
msgid "返回发货订单列表"
msgstr ""

#: view/sales/sales-send-order/view.phtml:23
msgid "销售发货单"
msgstr ""

#: view/sales/sales-send-order/view.phtml:133
msgid "发货仓库"
msgstr ""

#: view/sales/sales-send-order/view.phtml:135
msgid "发货数量："
msgstr ""

#: view/sales/sales-send-order/view.phtml:136
msgid "序列号："
msgstr ""

#: view/sales/translate.phtml:3
msgid "销售"
msgstr ""

#: view/sales/translate.phtml:5
msgid "订单列表"
msgstr ""

#: view/sales/translate.phtml:6
msgid "添加订单"
msgstr ""

#: view/sales/translate.phtml:7
msgid "编辑订单"
msgstr ""

#: view/sales/translate.phtml:8
msgid "查看订单详情"
msgstr ""

#: view/sales/translate.phtml:9
msgid "删除订单"
msgstr ""

#: view/sales/translate.phtml:10 view/sales/translate.phtml:12
msgid "删除订单内商品"
msgstr ""

#: view/sales/translate.phtml:11
msgid "确认订单"
msgstr ""

#: view/sales/translate.phtml:14
msgid "订单发货"
msgstr ""

#: view/sales/translate.phtml:15
msgid "销售发货"
msgstr ""

#: view/sales/translate.phtml:16
msgid "发货列表"
msgstr ""

#: view/sales/translate.phtml:17 view/sales/translate.phtml:22
msgid "查看详情"
msgstr ""

#: view/sales/translate.phtml:20
msgid "退货列表"
msgstr ""

#: view/sales/translate.phtml:21
msgid "添加退货"
msgstr ""
