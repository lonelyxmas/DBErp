<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Sales\Validator;

use Laminas\I18n\Translator\Translator;
use Laminas\Validator\AbstractValidator;

class InputSerialNumberStrValidator extends AbstractValidator
{
    const NOT_SCALAR        = 'notScalar';
    const SALES_GOODS_SERIAL_NUMBER_ERROR = 'salesGoodsSerialNumberError';

    protected $messageTemplates = [];

    private $sendOrderGoods;

    public function __construct($options = null)
    {
        $this->sendOrderGoods   = $options['sendOrderGoods'];

        $trans = new Translator();
        $this->messageTemplates = [
            self::NOT_SCALAR        => $trans->translate("这不是一个标准输入值"),
            self::SALES_GOODS_SERIAL_NUMBER_ERROR => $trans->translate("手动选择的商品序列号数量小于发货数量")
        ];

        parent::__construct($options);
    }

    public function isValid($value): bool
    {
        foreach ($this->sendOrderGoods as $goodsValue) {
            if (!empty($value[$goodsValue->getGoodsId()])) {
                $serialNumberN = count(array_filter(explode(',', $value[$goodsValue->getGoodsId()])));
                if ($serialNumberN < $goodsValue->getSalesGoodsSellNum()) {
                    $this->error(self::SALES_GOODS_SERIAL_NUMBER_ERROR);
                    return false;
                }
            }
        }

        return true;
    }
}