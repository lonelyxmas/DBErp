<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Report\Controller;

use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\I18n\Translator;
use Report\Form\SearchStockForm;
use Store\Entity\Warehouse;
use Store\Entity\WarehouseGoods;

class ReportStockController extends AbstractActionController
{
    private $translator;
    private $entityManager;

    public function __construct(
        Translator      $translator,
        EntityManager   $entityManager
    )
    {
        $this->translator       = $translator;
        $this->entityManager    = $entityManager;
    }

    public function indexAction()
    {
        $warehouseArray= [];
        $warehouseList = $this->entityManager->getRepository(Warehouse::class)->findBy([], ['warehouseSort' => 'ASC']);
        if ($warehouseList) foreach ($warehouseList as $warehouseValue) {
            $warehouseArray[] = [
                'warehouseName' => $warehouseValue->getWarehouseName(),
                'goodsType'     => $this->entityManager->getRepository(WarehouseGoods::class)->count(['warehouseId' => $warehouseValue->getWarehouseId()]),
                'goodsNum'      => $this->entityManager->getRepository(WarehouseGoods::class)->findWarehouseGoodsStock($warehouseValue->getWarehouseId())
            ];
        }

        $search = [];
        $searchForm = new SearchStockForm();
        $searchForm->get('warehouse_id')->setValueOptions($this->storeCommon()->warehouseListOptions());
        if($this->getRequest()->isGet()) {
            $data = $this->params()->fromQuery();
            $searchForm->setData($data);
            if($searchForm->isValid()) $search = $searchForm->getData();
        }

        $page = (int) $this->params()->fromQuery('page', 1);
        $query= $this->entityManager->getRepository(WarehouseGoods::class)->findWarehouseGoodsList($search);
        $warehouseGoodsList = $this->adminCommon()->erpPaginator($query, $page, 30);

        return [
            'searchForm' => $searchForm,
            'warehouseArray' => $warehouseArray,
            'warehouseGoodsList' => $warehouseGoodsList
        ];
    }
}