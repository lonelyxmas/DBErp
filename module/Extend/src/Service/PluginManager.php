<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Extend\Service;

use Doctrine\ORM\EntityManager;
use Extend\Entity\Plugin;

class PluginManager
{
    private $entityManager;

    public function __construct(
        EntityManager $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    /**
     * 添加插件
     * @param array $data
     * @return Plugin
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addPlugin(array $data): Plugin
    {
        $plugin = new Plugin();
        $plugin->valuesSet($data);

        $this->entityManager->persist($plugin);
        $this->entityManager->flush();

        return $plugin;
    }

    /**
     * 编辑插件
     * @param array $data
     * @param Plugin $plugin
     * @return Plugin
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editPlugin(array $data, Plugin $plugin): Plugin
    {
        $plugin->valuesSet($data);
        $this->entityManager->flush();

        return $plugin;
    }

    /**
     * 删除插件
     * @param Plugin $plugin
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deletePlugin(Plugin $plugin): bool
    {
        $this->entityManager->remove($plugin);
        $this->entityManager->flush();

        return true;
    }
}