<?php
/**
 * DBERP 进销存系统
 *
 * ==========================================================================
 * @link      http://www.dberp.net/
 * @copyright 北京珑大钜商科技有限公司，并保留所有权利。
 * @license   http://www.dberp.net/license.html License
 * ==========================================================================
 *
 * @author    静静的风 <baron@loongdom.cn>
 *
 */

namespace Extend\Entity;

use Admin\Entity\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * 插件
 * @ORM\Entity(repositoryClass="Extend\Repository\PluginRepository")
 * @ORM\Table(name="dberp_plugin")
 */
class Plugin extends BaseEntity
{
    /**
     * 自增id
     * @ORM\Id()
     * @ORM\Column(name="plugin_id", type="integer", length=11)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $pluginId;

    /**
     * 插件名称
     * @ORM\Column(name="plugin_name", type="string", length=100)
     */
    private $pluginName;

    /**
     * 插件作者
     * @ORM\Column(name="plugin_author", type="string", length=100)
     */
    private $pluginAuthor;

    /**
     * 插件作者网址
     * @ORM\Column(name="plugin_author_url", type="string", length=200)
     */
    private $pluginAuthorUrl;

    /**
     * 插件描述
     * @ORM\Column(name="plugin_info", type="text")
     */
    private $pluginInfo;

    /**
     * 插件版本，类似 V1.3
     * @ORM\Column(name="plugin_version", type="string", length=20)
     */
    private $pluginVersion;

    /**
     * 插件版本号，十位 如 20112700 年月日00
     * @ORM\Column(name="plugin_version_num", type="integer", length=11)
     */
    private $pluginVersionNum;

    /**
     * 插件标识码，如 goodsImport
     * @ORM\Column(name="plugin_code", type="string", length=50)
     */
    private $pluginCode;

    /**
     * 插件状态，1 启用，0 关闭
     * @ORM\Column(name="plugin_state", type="integer", length=1)
     */
    private $pluginState = 0;

    /**
     * 插件帮助url地址
     * @ORM\Column(name="plugin_support_url", type="string", length=200)
     */
    private $pluginSupportUrl;

    /**
     * 描述后台管理地址，如 系统-》系统设置
     * @ORM\Column(name="plugin_admin_path", type="string", length=200)
     */
    private $pluginAdminPath;

    /**
     * 更新时间，如 2020-11-23
     * @ORM\Column(name="plugin_update_time", type="string", length=10)
     */
    private $pluginUpdateTime;

    /**
     * @return mixed
     */
    public function getPluginId()
    {
        return $this->pluginId;
    }

    /**
     * @param mixed $pluginId
     */
    public function setPluginId($pluginId): void
    {
        $this->pluginId = $pluginId;
    }

    /**
     * @return mixed
     */
    public function getPluginName()
    {
        return $this->pluginName;
    }

    /**
     * @param mixed $pluginName
     */
    public function setPluginName($pluginName): void
    {
        $this->pluginName = $pluginName;
    }

    /**
     * @return mixed
     */
    public function getPluginAuthor()
    {
        return $this->pluginAuthor;
    }

    /**
     * @param mixed $pluginAuthor
     */
    public function setPluginAuthor($pluginAuthor): void
    {
        $this->pluginAuthor = $pluginAuthor;
    }

    /**
     * @return mixed
     */
    public function getPluginAuthorUrl()
    {
        return $this->pluginAuthorUrl;
    }

    /**
     * @param mixed $pluginAuthorUrl
     */
    public function setPluginAuthorUrl($pluginAuthorUrl): void
    {
        $this->pluginAuthorUrl = $pluginAuthorUrl;
    }

    /**
     * @return mixed
     */
    public function getPluginInfo()
    {
        return $this->pluginInfo;
    }

    /**
     * @param mixed $pluginInfo
     */
    public function setPluginInfo($pluginInfo): void
    {
        $this->pluginInfo = $pluginInfo;
    }

    /**
     * @return mixed
     */
    public function getPluginVersion()
    {
        return $this->pluginVersion;
    }

    /**
     * @param mixed $pluginVersion
     */
    public function setPluginVersion($pluginVersion): void
    {
        $this->pluginVersion = $pluginVersion;
    }

    /**
     * @return mixed
     */
    public function getPluginVersionNum()
    {
        return $this->pluginVersionNum;
    }

    /**
     * @param mixed $pluginVersionNum
     */
    public function setPluginVersionNum($pluginVersionNum): void
    {
        $this->pluginVersionNum = $pluginVersionNum;
    }

    /**
     * @return mixed
     */
    public function getPluginCode()
    {
        return $this->pluginCode;
    }

    /**
     * @param mixed $pluginCode
     */
    public function setPluginCode($pluginCode): void
    {
        $this->pluginCode = $pluginCode;
    }

    /**
     * @return mixed
     */
    public function getPluginState()
    {
        return $this->pluginState;
    }

    /**
     * @param mixed $pluginState
     */
    public function setPluginState($pluginState): void
    {
        $this->pluginState = $pluginState;
    }

    /**
     * @return mixed
     */
    public function getPluginSupportUrl()
    {
        return $this->pluginSupportUrl;
    }

    /**
     * @param mixed $pluginSupportUrl
     */
    public function setPluginSupportUrl($pluginSupportUrl): void
    {
        $this->pluginSupportUrl = $pluginSupportUrl;
    }

    /**
     * @return mixed
     */
    public function getPluginAdminPath()
    {
        return $this->pluginAdminPath;
    }

    /**
     * @param mixed $pluginAdminPath
     */
    public function setPluginAdminPath($pluginAdminPath): void
    {
        $this->pluginAdminPath = $pluginAdminPath;
    }

    /**
     * @return mixed
     */
    public function getPluginUpdateTime()
    {
        return $this->pluginUpdateTime;
    }

    /**
     * @param mixed $pluginUpdateTime
     */
    public function setPluginUpdateTime($pluginUpdateTime): void
    {
        $this->pluginUpdateTime = $pluginUpdateTime;
    }
}